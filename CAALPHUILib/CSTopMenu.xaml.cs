﻿using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace CAALPHUILib
{
    /// <summary>
    /// Interaction logic for CSTopMenu.xaml
    /// </summary>
    public partial class CSTopMenu : UserControl
    {
        public CSTopMenu()
        {
            InitializeComponent();
            HomeButton = homeButton;
        }

        public Canvas HomeButton { get; private set; }

        private void submenuButton_MouseEnter(object sender, MouseEventArgs e)
        {

            var bc = new BrushConverter();
            this.submenuButton.Background = (Brush)bc.ConvertFrom("#FF000000");

        }

        private void submenuButton_MouseLeave(object sender, MouseEventArgs e)
        {
            var bc = new BrushConverter();
            this.submenuButton.Background = (Brush)bc.ConvertFrom("#FF3DBFD9");
        }

        private void Image_MouseEnter(object sender, MouseEventArgs e)
        {
            var bc = new BrushConverter();
            this.homeButton.Background = (Brush)bc.ConvertFrom("#FF000000");
        }

        private void Image_MouseLeave(object sender, MouseEventArgs e)
        {
            var bc = new BrushConverter();
            this.homeButton.Background = (Brush)bc.ConvertFrom("#FFFFFFFF");
        }


    }
}
