﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;



namespace CAALPHUILib
{
    /// <summary>
    /// Interaction logic for CSClockWidget.xaml
    /// </summary>
    public partial class CSClockWidget : UserControl
    {

        public DispatcherTimer dispatcherTimer;



        public CSClockWidget()
        {
            InitializeComponent();

            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0,0,1);
            dispatcherTimer.Start();
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            // Updating the Label which displays the current second
            timeLabel.Text = DateTime.Now.ToString("HH:mm");
            secdayLabel.Text = ":"+DateTime.Now.ToString("ss")+" "+DateTime.Now.ToString("dddd");
            dateLabel.Text = DateTime.Now.ToString("d MMMM yyyy");
      
            // Forcing the CommandManager to raise the RequerySuggested event
            CommandManager.InvalidateRequerySuggested();
        }


    }
}
