﻿using caalhp.IcePluginAdapters.WPF;
using GalaSoft.MvvmLight.Messaging;

namespace HomeScreen.Views
{
    /// <summary>
    /// Description for MainView.
    /// </summary>
    public partial class MainView
    {
        /// <summary>
        /// Initializes a new instance of the MainView class.
        /// </summary>
        public MainView()
        {
            InitializeComponent();
            /*MarketTile.IconSource = "Images/app1.png";
            HealthDataTile.IconSource = "Images/app2.png";
            ManualDataInputTile.IconSource = "Images/app3.png";
            MarketTile2.IconSource = "Images/app1.png";
            HealthDataTile2.IconSource = "Images/app2.png";
            ManualDataInputTile2.IconSource = "Images/app3.png";*/
            //this.LogoImg.Source = new System.Windows.Media.DrawingImage();
            Messenger.Default.Register<NotificationMessage>(this, ShowView);
        }

        private void ShowView(NotificationMessage note)
        {
            if (note.Target.Equals(GetType()) && note.Notification.Equals("Show"))
            {
                Helper.BringToFront();
                //DispatcherHelper.CheckBeginInvokeOnUI(Show);
                //DispatcherHelper.CheckBeginInvokeOnUI(BringToFront);
            }
        }

        private void BringToFront()
        {
            Activate();
        }
        
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }
    }
}