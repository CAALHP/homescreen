﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Security.Permissions;
using System.Windows;
using System.Windows.Threading;
using caalhp.Core.Contracts;
using caalhp.Core.Events;
using caalhp.Core.Utils.Helpers;
using caalhp.Core.Utils.Helpers.Serialization;
using caalhp.Core.Utils.Helpers.Serialization.JSON;
using GalaSoft.MvvmLight.Messaging;
using HomeScreen.ViewModel;

// Does triggering work on the build server?

namespace HomeScreen
{
    public class HomeScreenImplementation : IAppCAALHPContract
    {
        private IAppHostCAALHPContract _host;
        private int _processId;
        private readonly MainViewModel _main;
        private const string AppName = "HomeScreen";

        public HomeScreenImplementation(MainViewModel mainViewModel)
        {
            //var vml = Application.Current.Resources["Locator"] as ViewModelLocator;
            //if (vml == null) return;
            _main = mainViewModel;
            Messenger.Default.Register<NotificationMessage>(this, SendStartAppMessage);
        }

        private void SendStartAppMessage(NotificationMessage message)
        {
            if (!message.Target.Equals(GetType())) return;
            var showAppEvent = new ShowAppEvent
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                AppName = message.Notification
            };
            var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, showAppEvent);
            _host.Host.ReportEvent(serializedEvent);
            _host.ShowApp(message.Notification);
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        private void HandleEvent(ShowAppEvent e)
        {
            if (e.AppName.Equals(AppName))
            {
                //Show homescreen
                Show();
            }
        }

        private void HandleEvent(InstallAppCompletedEvent e)
        {
            //Show homescreen
            Show();
        }

        public string GetName()
        {
            return AppName;
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            //Environment.Exit(0);
            Application.Current.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal);
        }

        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            //Subscribe to events so we can react to "Home" messages
            //_host.Host.SubscribeToEvents(_processId);
            //Debugger.Launch();
            Task.Run(()=>_host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(ShowAppEvent)), _processId));
            Task.Run(()=>_host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(InstallAppCompletedEvent)), _processId));

            Task.Run(()=>_host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserLoggedOutEvent)), _processId));
            _main.Start();
            Show();
        }

        private void HandleEvent(UserLoggedOutEvent obj)
        {
            try
            {
                Show();
            }
            catch
            {
                MessageBox.Show("Problem during UserLoggedOutEvent in HomeScreen");
            }
            
        }

        public void Show()
        {
            //ref: http://reedcopsey.com/2011/11/28/launching-a-wpf-window-in-a-separate-thread-part-1/
            //Threading is used so we do not block the main thread.
            if (_main != null)
                DispatchToApp(() => _main.Show());
        }
        
        public IList<IPluginInfo> GetListOfInstalledApps()
        {
            return _host.GetListOfInstalledApps();
        }

        private void DispatchToApp(Action action)
        {
            Application.Current.Dispatcher.Invoke(action, DispatcherPriority.Normal);
        }

        [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
        public void DoTheMessagePump()
        {
            DispatcherFrame frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
                new DispatcherOperationCallback(ExitFrame), frame);
            Dispatcher.PushFrame(frame);
        }

        public object ExitFrame(object f)
        {
            ((DispatcherFrame)f).Continue = false;

            return null;
        }

    }
}